// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;



const WALKER = 1;
const DRIVER = 2;


// Routing
app.use(express.static(__dirname + '/public'));

var driver1 = {
      username: "Driver Username 1",
      name: "Driver name 1",
      phonenumber: "9845986456",
      location: {
        latitude : 33.2,
        longitude : -6.2
      }
    };
var driver2 = {
      username: "Driver Username 2",
      name: "Driver name 2",
      phonenumber: "051961651",
      location: {
        latitude : 33.1,
        longitude : -6.1
      }
    };

var driver3 = {
      username: "Driver Username 3",
      name: "Driver name 3",
      phonenumber: "05196165fsdgs",
      location: {
        latitude : 32.9,
        longitude : -6.3
      }
    };


var drivers = [];



var walkers = [];

var numWalkers = 0;
var numDrivers = 3;


io.on('connection', function (socket) {
	console.log('Connected');
  console.log(numDrivers + " ------ " + numWalkers);

  //var addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add walker', function (data) {

    ++numWalkers;
    socket.join('walkers');
    socket.category = data.category;
    console.log(" socket.category "+socket.category);
  	socket.username = data.username;
    walkers[socket.username] = socket; 

    var walkerObj = {
      username: data.username,
      socketid: socket.id,
      name: data.name,
      phonenumber: data.phonenumber,
      location: {
        latitude : data.location.latitude,
        longitude : data.location.longitude
      }
    };
    console.log('add walker2');
    walkers.push(walkerObj);
    //socket.emit('test')
    io.emit('drivers around', drivers);
  });

  
  // when the client emits 'add user', this listens and executes
  socket.on('add driver', function (data) {

    ++numDrivers;
    socket.join('drivers');
    socket.category = data.category;
    socket.username = data.username;
    drivers[socket.username] = socket; 
    var driverObj = {
      username: data.username,
      socketid: socket.id,
      name: data.name,
      phonenumber: data.phonenumber,
      location: {
        latitude : data.location.latitude,
        longitude : data.location.longitude
      }
    };
    
    drivers.push(driverObj);
    console.log('here1');
    //socket.broadcast.emit('driver connected', driver);

    socket.broadcast.to('walkers').emit('driver connected', driverObj);
  });

  socket.on('ask driver', function (data) {

    console.log('WALKERS = '+walkers.length);
    console.log('Drivers = '+drivers.length);


    var currentWalker = walkers.find(function(item) {
      return item.username === socket.username;
    });

    currentWalker.destication = data.destination;

    console.log('ask driver --userName' + currentWalker.username);

    var driverToAsk = drivers.find(function(item) {
      return item.username === data.username;
    });

    socket.broadcast.to(driverToAsk.socketid).emit('request received', currentWalker);
   //io.emit('show walker', currentWalker)

  });

  socket.on('update location', function (data) {

    if(data.category == WALKER ){

      for (var i=0; i<walkers.length; i++) {
        if (walkers[i].username == data.username) {
          walkers[i].location = data.location;
          break;
          }
      }
      socket.broadcast.to('drivers').emit('update walker location', data);

    }else {
      for (var i=0; i<drivers.length; i++) {
        if (drivers[i].username == data.username) {
          drivers[i].location = data.location;
          break;
          }
      }
      socket.broadcast.to('walkers').emit('update driver location', data);
    }

    /////////
  });







  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if(socket.category == WALKER ){
      --numWalkers;
      walkers = walkers.filter(function(item) {
      return item.username !== socket.username;
    });
      socket.broadcast.to('drivers').emit('walker disconnected', { username: socket.username});


    }else {
      --numDrivers;
      drivers = drivers.filter(function(item) {
      return item.username !== socket.username;
    });
      //io.emit('driver disconnected', { username: socket.username});
      socket.broadcast.to('walkers').emit('driver disconnected', { username: socket.username});
    }
console.log(numDrivers + " ------ " + numWalkers);
      
    
  });
    
    

});

server.listen(port, function () {
  console.log('Server listening at port %d', port);
  console.log('here');
});